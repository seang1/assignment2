/* Names: Sean Gajjar, David Emelianov; Lab: M 4:30-6p */
package assignment2;

import java.util.*;
import java.lang.*;
import java.io.*;

/* Name of the class has to be "Main" only if the class is public. */
class Game
{
	static boolean debug;
	public Game(boolean debug2) {
		// TODO Auto-generated constructor stub
		debug = debug2;
	}

	public Game() {
		// TODO Auto-generated constructor stub
	}

	public static void runGame() {
		System.out.println("Let's play Mastermind. Guess a 4 color combo of blue, green, orange, purple, red, or yellow");
		
	
//set up an array with all possible answers. We'll use this to check inputs later.
		ArrayList<Character> validAnswers = new ArrayList<Character>(); 
		validAnswers.add('B');
		validAnswers.add('G');
		validAnswers.add('O');
		validAnswers.add('P');
		validAnswers.add('R');
		validAnswers.add('Y');

//set up a random solution (correctCode)
		ArrayList<Character> correctCode = codeGen();
		
//this loop is used every time the user guesses something
		ArrayList<String> history = new ArrayList<String>(); 
		for (int i = 12; i > 0; i--) {
			if (debug == true) 
			{
				System.out.println(correctCode);
			}
			
			System.out.println("You have " + i + " guesses left");

//read in the guess
			String usersGuess = keyInput();

//check the guess for correct length
			if (usersGuess == null) { 
				System.out.println("INVALID GUESS");
				i++;
			}
	//		else if(usersGuess.equals))
			else if(usersGuess.equals("history"))
			{
				if(history.isEmpty())
				{
					System.out.println("You haven't made any guesses");
				}
				else{
				System.out.println("History of Guesses");
				System.out.println("Guess Number		Guess			Result");	

				for(String line: history)
				{
					System.out.println(line);
				}
				}
				i++;
				
			}
			else if (usersGuess.length() != 4) { 
				System.out.println("INVALID GUESS");
				i++;
			}
//now we know that the guess is the correct length, so let's split up the input and check for valid characters
			else 
			{
				char firstChar = usersGuess.charAt(0);
				char secondChar = usersGuess.charAt(1);
				char thirdChar = usersGuess.charAt(2);
				char fourthChar = usersGuess.charAt(3);
//check the guess for correct values
				if ( validAnswers.contains(firstChar) && validAnswers.contains(secondChar) && validAnswers.contains(thirdChar) && validAnswers.contains(fourthChar) )
				{
					pegFinder(correctCode, firstChar, secondChar, thirdChar,
							fourthChar, history, i, usersGuess);
				}
//this else statement is triggered if invalid characters are used. Let's give the guess back and repeat.
				else 
				{
					System.out.println("INVALID GUESS");
					i++;
				}
			}
		}
	}

	public static ArrayList<Character> codeGen() {
		ArrayList<Character> correctCode = new ArrayList<Character>(); //changed this to char Array
		Random rand = new Random();
		for (int i=0; i<4; i++) {
			int x = rand.nextInt(6);
			if (x==0) { correctCode.add('B'); }
			if (x==1) { correctCode.add('G'); }
			if (x==2) { correctCode.add('O'); }
			if (x==3) { correctCode.add('P'); }
			if (x==4) { correctCode.add('R'); }
			if (x==5) { correctCode.add('Y'); }
		}
		return correctCode;
	}

	public static void pegFinder(ArrayList<Character> correctCode,
			char firstChar, char secondChar, char thirdChar, char fourthChar, ArrayList<String> history, int i, String usersGuess) {
		ArrayList<Character> correctCopy = new ArrayList<Character>(correctCode);
		
		int whiteReplace = 0;
		int blackPeg = 0;
		int whitePeg = 0;
// WE HAVE A VALID GUESS!!! Time to check it against "correctCode" and print the black/white pins
// If the guess turns out to be exactly right, break out of the loop
		if(firstChar == correctCopy.get(0))
		{
			correctCopy.set(0, '0');
			firstChar = '!';
			blackPeg++;
		}
		if(secondChar == correctCopy.get(1))
		{
			correctCopy.set(1, '0');
			secondChar = '!';
			blackPeg++;
		}
		if(thirdChar == correctCopy.get(2))
		{
			correctCopy.set(2, '0');
			thirdChar = '!';
			blackPeg++;
		}
		if(fourthChar == correctCopy.get(3))
		{
			correctCopy.set(3, '0');
			fourthChar = '!';
			blackPeg++;
		}
		
		
		//if all four pegs are black you WON!
		if(blackPeg == 4)
		{
			System.out.println("Result: 4 black pegs - You win!");
			playAgain();
			
		}
		
		if(correctCopy.contains(firstChar))
		{
			whiteReplace = correctCopy.indexOf(firstChar);
			correctCopy.set(whiteReplace, '/');
			whitePeg++;
		}
		if(correctCopy.contains(secondChar))
		{
			whiteReplace = correctCopy.indexOf(secondChar);
			correctCopy.set(whiteReplace, '/');
			whitePeg++;
		}
		if(correctCopy.contains(thirdChar))
		{
			whiteReplace = correctCopy.indexOf(thirdChar);
			correctCopy.set(whiteReplace, '/');
			whitePeg++;					
		}
		if(correctCopy.contains(fourthChar))
		{
			whiteReplace = correctCopy.indexOf(fourthChar);
			correctCopy.set(whiteReplace, '/');
			whitePeg++;
		}
		//now you know how many White Pegs you have
		//print out the result of the pegs next
		int guessNum = 13-i;
		if(blackPeg == 0 && whitePeg ==0)
		{
			System.out.println("Result: no pegs");
			history.add(guessNum + "			"+ usersGuess + "			" + "No pegs");
		}
		else
		{
		System.out.println("Result: " + blackPeg + " Black Pegs and " + whitePeg + " White Pegs");
		history.add(guessNum + "			"+ usersGuess + "			" + blackPeg +" black peg(s)," + whitePeg + " white peg(s)");
		}
	}

	public static void playAgain() {
		System.out.println("Are you ready for another game (Y/N): ");
		Scanner reset = new Scanner(System.in);
		String resett = reset.nextLine().trim();
		if(resett.equals("N"))
		{
			System.exit(0);
		}
		else if(resett.equals("Y"))
		{
			runGame();
		}
		else
		{
			System.out.println("Invalid entry! Type Y or N ");
			playAgain();
		}
	}

	public static String keyInput() {
		System.out.println("What is your next guess?");
		System.out.println("Type in the characters for your guess and press enter");
		Scanner scanIn = new Scanner(System.in);
		String usersGuess = null;
		usersGuess = scanIn.nextLine();
		return usersGuess;
	}
}